//
//  NXAppCoordinator.swift
//  NXBox
//
//  Created by Anton Shelar on 27/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


class NXAppCoordinator {
    private let window: UIWindow
    private(set) var currentRouter: NXRouter!

    init(window: UIWindow) {
        self.window = window
    }

    func start(_ route: NXRoute? = nil) {
        assertionFailure("You must override \(#function) in your subclass")
    }

    @discardableResult
    func handleRoute(_ route: NXRoute) -> Bool {
        return self.currentRouter.handleRoute(route)
    }

    func setInitialRouter(_ router: NXRouter) {
        self.currentRouter = router

        self.window.rootViewController = router.navigationController
        self.window.makeKeyAndVisible()
    }
}


