//
//  NXRoute.swift
//  NXBox
//
//  Created by Anton Shelar on 27/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation


enum NXRoute {
    init?(url: URL) {
        return nil
    }

    init?(notification: [AnyHashable: Any]) {
        return nil
    }
}
