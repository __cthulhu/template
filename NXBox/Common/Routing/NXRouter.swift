//
//  NXRouter.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


protocol NXRouteHandler {
    func handleRoute(_ route: NXRoute) -> Bool
}


class NXRouter: NXRouteHandler {
    let navigationController: UINavigationController = UINavigationController()
    var completionHandler: (() -> Void)?
    private(set) var childRouters: [NXRouter] = []

    func start() { }

    func addChildRouter(_ router: NXRouter) {
        for r: NXRouter in self.childRouters {
            if r === router {
                assertionFailure("Can't add the same router as a child")
                return
            }
        }

        self.childRouters.append(router)
    }

    func viewControllerFromNavigationStack<T: UIViewController>(_ vcType: T.Type) -> T? {
        return self.navigationController.viewControllers.reversed().first(where: { (vc: UIViewController) -> Bool in
            return (vc as? T) != nil
        }) as? T
    }
    
    func presentChild(_ router: NXRouter, presentationStyle: UIModalPresentationStyle = .fullScreen) {
        router.navigationController.modalPresentationStyle = presentationStyle

        self.navigationController.present(router.navigationController, animated: true, completion: nil)
    }

    func dismiss(animated: Bool = true, completion: (() -> Void)? = nil) {
        self.navigationController.dismiss(animated: animated, completion: completion)
    }

    func currentViewController() -> UIViewController {
        return (self.navigationController.topViewController?.presentedViewController ?? self.navigationController.topViewController) ?? self.navigationController
    }

    @discardableResult
    func popToViewControllerIfExist<T: UIViewController>(_ vcType: T.Type, animated: Bool = true) -> Bool {
        if let vc: T = self.viewControllerFromNavigationStack(T.self) {
            self.navigationController.popToViewController(vc, animated: animated)
            return true
        } else {
            return false
        }
    }

    @discardableResult
    func handleRoute(_ route: NXRoute) -> Bool {
        for router: NXRouter in self.childRouters {
            if router.handleRoute(route) {
                return true
            }
        }

        return false
    }
}

