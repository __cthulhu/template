//
//  NXGoogleSignIn.swift
//  nxnews
//
//  Created by fhtagn on 22.03.2020.
//  Copyright © 2020 NXCOM. All rights reserved.
//

import Foundation
import GoogleSignIn
import UIKit


class NXGoogleSignIn: NSObject, GIDSignInDelegate {
    private var handler: NXOpenURLHandler?
    private var completion: ((GIDGoogleUser) -> Void)?

    init(clientId: String) {
        super.init()

        GIDSignIn.sharedInstance()?.clientID = clientId
        GIDSignIn.sharedInstance()?.delegate = self
    }

    func signIn(presentingViewController: UIViewController, completion: ((GIDGoogleUser) -> Void)?) {
        self.completion = completion

        self.handler = NXOpenURLHandler { (url: URL) -> Bool in
            return (GIDSignIn.sharedInstance()?.handle(url) ?? false)
        }
        
        NXFactory.shared.urlHandlers.addHandler(self.handler!)

        GIDSignIn.sharedInstance()?.presentingViewController = presentingViewController
        GIDSignIn.sharedInstance()?.signIn()
    }

    func signOut() {
        GIDSignIn.sharedInstance()?.signOut()
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let handler: NXOpenURLHandler = self.handler {
            NXFactory.shared.urlHandlers.removeHandler(handler)
        }

        if let user = user {
            self.completion?(user)
        }

        self.completion = nil
    }
}
