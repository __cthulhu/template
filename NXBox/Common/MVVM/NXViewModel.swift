//
//  NXViewModel.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation


class NXViewModel {
    let isLoading: NXObservable<Bool> = NXObservable<Bool>(false)
}
