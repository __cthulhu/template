//
//  NXObservable.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation


fileprivate class NXWeakObject {
    weak var object: AnyObject?

    init(object: AnyObject?) {
        self.object = object
    }
}


class NXObservable<T> {
    private let isSingleObservable: Bool

    var isEnabled: Bool = true
    var value: T {
        didSet {
            if !self.isEnabled {
                return
            }

            self.weakObservers = self.weakObservers.filter{observer in observer.owner.object != nil}

            for observer in self.weakObservers {
                observer.callbackBlock(self.value)
            }
        }
    }

    private var weakObservers: [(owner: NXWeakObject, callbackBlock: ((T) -> Void))] = []

    init(_ value: T) {
        self.isSingleObservable = false
        self.value = value
    }

    fileprivate init(_ value: T, isSingleObservable: Bool) {
        self.isSingleObservable = isSingleObservable
        self.value = value
    }

    func addObserver(owner: AnyObject,
                     callNow: Bool = true,
                     callbackBlock: @escaping ((T) -> Void)) {

        if self.isSingleObservable && self.weakObservers.count != 0 {
            assertionFailure("Attempting to add another observer to a single-observable variable")
        }

        self.weakObservers.append((owner: NXWeakObject(object: owner), callbackBlock: callbackBlock))

        if callNow {
            callbackBlock(self.value)
        }
    }

    func removeAllObservers() {
        self.weakObservers = []
    }

    func notifyAllObservers() {
        for observer in self.weakObservers {
            observer.callbackBlock(self.value)
        }
    }
}


class NXSingleObservable<T>: NXObservable<T> {
    override init(_ value: T) {
        super.init(value, isSingleObservable: true)
    }
}
