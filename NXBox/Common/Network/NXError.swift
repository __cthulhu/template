//
//  NXError.swift
//  NXBox
//
//  Created by Anton Shelar on 27/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation


class NXError: Error, LocalizedError {
    private let message: String

    var errorDescription: String? {
        return self.message
    }

    var failureReason: String? {
        return self.message
    }

    var recoverySuggestion: String? {
        return nil
    }

    var helpAnchor: String? {
        return nil
    }

    init(localizedDescription: String) {
        self.message = localizedDescription
    }
}
