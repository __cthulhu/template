//
//  NXResult.swift
//  NXBox
//
//  Created by Anton Shelar on 27/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation


enum Result<T> {
    case success(T?)
    case fail(Error)
}
