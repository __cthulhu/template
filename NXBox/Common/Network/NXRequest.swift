//
//  NXRequest.swift
//  NXBox
//
//  Created by Anton Shelar on 27/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

// TODO: add network requests logging


import Foundation


enum NXRequestMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case delete = "DELETE"
}


class Request<T: Decodable> {
    var basePath: String = ""
    var method: NXRequestMethod
    var timeout: TimeInterval = 30.0
    var rootKey: String? = "data"
    let errorsKey: String = "errors"
    let successKey: String = "success"

    init() {
        self.method = .get
    }

    func perform(completion: ((Result<T>) -> Void)?) {
        guard let request: URLRequest = self.request() else {
            completion?(Result<T>.fail(NXError(localizedDescription: "Can't create URL request")))
            return
        }

        /* if !Reachability.connected() {
             completion?(.fail(NXError(localizedDescription: "No network connection")))
             return
         } */

        print(request.httpBody ?? "")

        let session: URLSession = self.session()

        let task: URLSessionDataTask = session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in

            DispatchQueue.main.async {
                completion?(self.parseReponse(response: response, data: data, error: error))
            }
        }

        task.resume()
    }

    func request() -> URLRequest? {
        guard let url = self.url() else {
            assertionFailure("Can't create URLRequest")
            return nil
        }

        var request: URLRequest = URLRequest(url: url)
        request.timeoutInterval = self.timeout
        request.httpMethod = self.method.rawValue

        return request
    }

    func url() -> URL? {
        return URL(string: "\(self.basePath)/\(self.relativePath())")
    }

    func session() -> URLSession {
        return URLSession(configuration: .default)
    }

    func relativePath() -> String {
        return ""
    }

    func parseReponse(response: URLResponse?, data: Data?, error: Error?) -> Result<T> {
        print(response ?? "")

        if let error = error {
            return .fail(error)
        }

        guard let data = data else {
            return .success(nil)
        }

        guard let dict: NSDictionary = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)) as? NSDictionary else {
            assertionFailure("Invalid data")
            return .fail(NXError(localizedDescription: "Invalid data"))
        }

        print(dict)

        if let responseError: Error = self.extractError(dict: dict) {
            return .fail(responseError)
        }

        if !self.isSuccess(dict: dict) {
            return .fail(NXError(localizedDescription: "Unknown error"))
        }

        if let rootKey: String = self.rootKey {
            let data: Any? = dict.object(forKey: rootKey)

            guard let keyData: [String: Any?] = data as? [String: Any?] else {
                assertionFailure("No data for key '\(rootKey)'")
                return .fail(NXError(localizedDescription: "No data for key '\(rootKey)'"))
            }

            guard let jsonData: Data = try? JSONSerialization.data(withJSONObject: keyData, options: .init()) else {
                assertionFailure("Invalid data")
                return .fail(NXError(localizedDescription: "Invalid data"))
            }

            return .success(try? JSONDecoder().decode(T.self, from: jsonData))
        } else {
            return .success(try? JSONDecoder().decode(T.self, from: data))
        }
    }

    func extractError(dict: NSDictionary) -> Error? {
        assertionFailure("Override \(#function) in your subclass :3")
        return nil
    }

    func isSuccess(dict: NSDictionary) -> Bool {
        assertionFailure("Override \(#function) in your subclass :3")
        return false
    }
}
