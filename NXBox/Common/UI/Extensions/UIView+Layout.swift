//
//  UIView+Layout.swift
//  NXBox
//
//  Created by Anton Shelar on 22/07/2019.
//  Copyright © 2019 Quaraseeque. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    func fillContainer(_ container: UIView, spacings: UIEdgeInsets = .zero) {
        if self.superview == nil {
            assertionFailure("The view must be added to view hierarchy")
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false

        self.leftAnchor.constraint(equalTo: container.leftAnchor, constant: spacings.left).isActive = true
        self.topAnchor.constraint(equalTo: container.topAnchor, constant: spacings.top).isActive = true
        self.rightAnchor.constraint(equalTo: container.rightAnchor, constant: spacings.right).isActive = true
        self.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: spacings.bottom).isActive = true
    }
    
    func centerInContainer(_ container: UIView) {
        if self.superview == nil {
            assertionFailure("The view must be added to view hierarchy")
        }
        
        self.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
        self.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
    }

    func fillSuperview(spacings: UIEdgeInsets = .zero) {
        self.fillContainer(self.superview!, spacings: spacings)
    }
}
