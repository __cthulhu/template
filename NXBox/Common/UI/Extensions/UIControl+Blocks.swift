//
//  UIControl+Blocks.swift
//  NXBox
//
//  Created by Anton Shelar on 22/07/2019.
//  Copyright © 2019 Quaraseeque. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC


fileprivate var _actionsKey: Void?

extension UIControl {
    private var actions: [UIControl.Event: (() -> Void)?]? {
        get {
            return (objc_getAssociatedObject(self, &_actionsKey) as? [UIControl.Event: (() -> Void)?]) ?? [:]
        }

        set {
            objc_setAssociatedObject(self, &_actionsKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    func setAction(_ event: UIControl.Event, execute: (() -> Void)?) {
        switch event {
        case UIControl.Event.touchUpInside:
            self.addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        default:
            assertionFailure("Not implemented for event '\(event)'")
            return
        }

        var actions: [UIControl.Event: (() -> Void)?] = self.actions ?? [:]
        actions[event] = execute

        self.actions = actions
    }

    func setTapAction(_ action: (() -> Void)?) {
        self.setAction(.touchUpInside, execute: action)
    }

    @objc
    private func touchUpInside() {
        self.actions?[UIControl.Event.touchUpInside]??()
    }
}


extension UIControl.Event: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.rawValue)
    }
}
