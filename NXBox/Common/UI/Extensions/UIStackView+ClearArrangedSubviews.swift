//
//  UIStackView+ClearArrangedSubviews.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


extension UIStackView {
    func clear() {
        for view in self.arrangedSubviews {
            self.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
}
