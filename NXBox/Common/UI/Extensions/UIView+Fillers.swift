//
//  UIView+Fillers.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    static func createHFiller() -> UIView {
        let filler: UIView = UIView()
        filler.setContentHuggingPriority(.defaultLow, for: .horizontal)
        filler.backgroundColor = .clear

        return filler
    }

    static func createVFiller() -> UIView {
        let filler: UIView = UIView()
        filler.setContentHuggingPriority(.defaultLow, for: .vertical)
        filler.backgroundColor = .clear

        return filler
    }

    static func containerForView(_ view: UIView, spacings: UIEdgeInsets = .zero) -> UIView {
        let container: UIView = UIView()
        container.addSubview(view)
        container.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false

        view.leftAnchor.constraint(equalTo: container.leftAnchor, constant: spacings.left).isActive = true
        view.topAnchor.constraint(equalTo: container.topAnchor, constant: spacings.top).isActive = true
        view.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -spacings.right).isActive = true
        view.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -spacings.bottom).isActive = true

        return container
    }
}
