//
//  UIImage+Color.swift
//  NXBox
//
//  Created by Anton Shelar on 22/07/2019.
//  Copyright © 2019 Quaraseeque. All rights reserved.
//

import Foundation
import UIKit


extension UIImage {
    static func fromColor(color: UIColor) -> UIImage? {
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)

        UIGraphicsBeginImageContext(rect.size)

        let context: CGContext? = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)

        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return image
    }
}

