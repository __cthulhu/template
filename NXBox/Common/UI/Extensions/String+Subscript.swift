//
//  String+Subscript.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import Foundation


extension String {
    subscript (i: Int) -> Character {
        return self[index(self.startIndex, offsetBy: i)]
    }
}
