//
//  NXViewController.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import UIKit


class NXViewController<T: NXViewModel>: UIViewController {
    private(set) var vm: T!
    var shouldHideNavigationBar: Bool = false

    init() {
        super.init(nibName: NSStringFromClass(type(of: self)).components(separatedBy: ".").last,
                   bundle: nil)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: NSStringFromClass(type(of: self)).components(separatedBy: ".").last,
                   bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(nibName: NSStringFromClass(type(of: self)).components(separatedBy: ".").last,
                   bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(image: self.leftBarButtonImage(), style: .plain, target: self, action: #selector(leftBarButtonAction)), animated: false)

        self.navigationItem.setRightBarButton(UIBarButtonItem(image: self.rightBarButtonImage(), style: .plain, target: self, action: #selector(rightBarButtonAction)), animated: false)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(_keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(_keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.shouldHideNavigationBar {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }

    func bindViewModel(_ vm: T) {
        self.loadViewIfNeeded()

        self.vm = vm

        vm.isLoading.addObserver(owner: self) { [weak self] (loading: Bool) in
            self?.setLoaderHidden(loading)
        }
    }
    
    func keyboardWillShow(frame: CGRect) { }

    func keyboardWillHide() { }
    
    func leftBarButtonImage() -> UIImage? {
      return nil
    }

    func rightBarButtonImage() -> UIImage? {
      return nil
    }
    
    @objc
    func leftBarButtonAction() {
      self.navigationController?.popViewController(animated: true)
    }

    @objc
    func rightBarButtonAction() { }

    func setLoaderHidden(_ hidden: Bool) { }

    @objc
    private func _keyboardWillShow(notification: Notification) {
        if let frame: CGRect = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            self.keyboardWillShow(frame: frame)
        }
    }

    @objc
    private func _keyboardWillHide(notification: Notification) {
        self.keyboardWillHide()
    }
}

