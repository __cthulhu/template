//
//  NXXIBView.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import UIKit


class NXXIBView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.loadXIB()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.loadXIB()
    }

    func xibName() -> String? {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last
    }

    func loadXIB() {
        guard let xibName: String = self.xibName() else {
            return
        }

        guard let view: UIView = Bundle.main.loadNibNamed(xibName,
                                                          owner: self,
                                                          options: nil)?[0] as? UIView else {
            assertionFailure("can't load view")
            return
        }

        view.translatesAutoresizingMaskIntoConstraints = false;
        self.addSubview(view)

        self.addConstraints([
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])

        self.backgroundColor = UIColor.clear
    }
}
