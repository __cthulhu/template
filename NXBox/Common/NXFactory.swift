//
//  NXFactory.swift
//  nxnews
//
//  Created by fhtagn on 22.03.2020.
//  Copyright © 2020 NXCOM. All rights reserved.
//

import Foundation


class NXFactory {
    static let shared: NXFactory = NXFactory()

    let urlHandlers: NXOpenURLHandlers<NXOpenURLHandler> = NXOpenURLHandlers<NXOpenURLHandler>()
}
