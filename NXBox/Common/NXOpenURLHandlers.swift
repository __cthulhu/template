//
//  NXOpenURLHandlers.swift
//  nxnews
//
//  Created by fhtagn on 22.03.2020.
//  Copyright © 2020 NXCOM. All rights reserved.
//

// TODO: alpha

import Foundation


class NXOpenURLHandlers<T: NXOpenURLHandler> {
    private var handlers: [T] = []

    func addHandler(_ handler: T) {
        if self.isHandlerExist(handler) {
            assertionFailure("Attempting to add existing NXOpenURLHandler. It's illegal.")
            return
        }

        self.handlers.append(handler)
    }

    func handleURL(_ url: URL) -> Bool {
        for h: T in self.handlers {
            if h.handlerBlock(url) {
                return true
            }
        }

        return false
    }

    func isHandlerExist(_ handler: T) -> Bool {
        for h: T in self.handlers {
            if h === handler {
                return true
            }
        }

        return false
    }

    func removeHandler(_ handler: T) {
        for (i, h) in self.handlers.enumerated() {
            if h === handler {
                self.handlers.remove(at: i)
                return
            }
        }
    }
}


class NXOpenURLHandler {
    let handlerBlock: ((URL) -> Bool)

    init(block: @escaping ((URL) -> Bool)) {
        self.handlerBlock = block
    }
}
