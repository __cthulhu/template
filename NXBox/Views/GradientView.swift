//
//  GradientView.swift
//  nxnews
//
//  Created by Anton Shelar on 18.03.2020.
//  Copyright © 2020 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


class GradientView: UIView {
    private let gradientLayer: CAGradientLayer = CAGradientLayer()

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.setUp()
    }

    private func setUp() {
        self.layer.insertSublayer(self.gradientLayer, at: 0)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        self.gradientLayer.frame = self.bounds
    }

    func setColors(_ colors: [CGColor],
                   startPoint: CGPoint = CGPoint(x: 0.5, y: 1.0),
                   endPoint: CGPoint = CGPoint(x: 0.5, y: 0.0)) {
        self.gradientLayer.colors = colors
        self.gradientLayer.startPoint = startPoint
        self.gradientLayer.endPoint = endPoint
    }
}
