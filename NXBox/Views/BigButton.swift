//
//  BigButton.swift
//  nxnews
//
//  Created by Anton Shelar on 18.03.2020.
//  Copyright © 2020 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


class BigButton: UIButton {
    private let spacing: CGFloat = 6.0

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.setUp()
    }

    private func setUp() {
        self.layer.cornerRadius = 5.0

        let heightConstraint: NSLayoutConstraint = self.heightAnchor.constraint(equalToConstant: 55.0)
        heightConstraint.priority = UILayoutPriority(999)
        heightConstraint.isActive = true

        self.setStyle(.solid(titleColor: .white, backgroundColor: R.color.neonRed()))
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        var titleRect: CGRect = self.titleLabel?.frame ?? .zero
        var imageRect: CGRect = self.imageView?.frame ?? .zero

        imageRect.origin.y = (self.frame.size.height - imageRect.size.height) / 2.0
        imageRect.origin.x = (self.frame.size.width - (imageRect.size.width + titleRect.size.width + (titleRect.size.width > 0 ? self.spacing : 0))) / 2.0

        self.imageView?.frame = imageRect

        titleRect.origin.y = (self.frame.size.height - titleRect.size.height) / 2.0
        titleRect.origin.x = imageRect.origin.x + imageRect.size.width + (imageRect.size.width > 0 ? self.spacing : 0)

        self.titleLabel?.frame = titleRect
    }

    func setStyle(_ style: Style) {
        switch style {
        case .bordered(let titleColor, let borderColor):
            self.layer.borderColor = borderColor?.cgColor
            self.layer.borderWidth = 1.0
            self.backgroundColor = UIColor.clear
            self.setTitleColor(titleColor, for: .init())
            break
        case .solid(let titleColor, let backgroundColor):
            self.layer.borderColor = nil
            self.layer.borderWidth = 0.0
            self.backgroundColor = backgroundColor
            self.setTitleColor(titleColor, for: .init())
        case .plain(let titleColor):
            self.layer.borderColor = nil
            self.layer.borderWidth = 0.0
            self.backgroundColor = UIColor.clear
            self.setTitleColor(titleColor, for: .init())
            break
        }
    }

    enum Style {
        case solid(titleColor: UIColor?, backgroundColor: UIColor?)
        case bordered(titleColor: UIColor?, borderColor: UIColor?)
        case plain(titleColor: UIColor?)
    }
}
