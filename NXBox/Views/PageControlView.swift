//
//  PageControlView.swift
//  nxnews
//
//  Created by Anton Shelar on 18.03.2020.
//  Copyright © 2020 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


class PageControlView: UIView {
    private let activePageColor: UIColor? = R.color.neonRed()
    private let inactivePageColor: UIColor? = UIColor.clear
    private let inactivePageBorderColor: UIColor? = UIColor.white
    private var pagesCount: Int = 0
    private var currentPage: Int = 0
    private var pageSideSize: CGFloat = 8.0
    private var activePageBorderWidth: CGFloat = 1.0
    private var pagesSpacing: CGFloat = 8.0
    private let stackView: UIStackView = UIStackView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.setUp()
    }

    private func setUp() {
        self.addSubview(self.stackView)
        self.stackView.fillSuperview()
        self.stackView.spacing = self.pagesSpacing
    }

    func setPagesCount(_ count: Int) {
        self.pagesCount = count

        self.setPages()
    }

    func setCurrentPage(_ page: Int) {
        self.currentPage = page

        for (i, view) in self.stackView.arrangedSubviews.enumerated() {
            view.backgroundColor = ((i == page) ? self.activePageColor : self.inactivePageColor)
            view.layer.borderColor = ((i == page) ? nil : self.inactivePageBorderColor)?.cgColor
            view.layer.borderWidth = ((i == page) ? 0.0 : self.activePageBorderWidth)
        }
    }

    private func setPages() {
        self.stackView.clear()

        if self.pagesCount == 0 {
            return
        }

        for _ in 0...(self.pagesCount - 1) {
            let view: UIView = UIView()
            view.backgroundColor = self.inactivePageColor
            view.layer.cornerRadius = self.pageSideSize / 2.0

            view.translatesAutoresizingMaskIntoConstraints = false
            view.widthAnchor.constraint(equalToConstant: self.pageSideSize).isActive = true
            view.heightAnchor.constraint(equalToConstant: self.pageSideSize).isActive = true


            self.stackView.addArrangedSubview(view)
        }
    }
}
