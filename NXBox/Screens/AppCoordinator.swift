//
//  AppCoordinator.swift
//  nxnews
//
//  Created by Anton Shelar on 06.03.2020.
//  Copyright © 2020 Anton Shelar. All rights reserved.
//

import UIKit

class AppCoordinator: NXAppCoordinator {
    let onboardingRouter: OnboardingRouter = OnboardingRouter()
    
    override func start(_ route: NXRoute? = nil) {
        self.onboardingRouter.start()
        
        self.setInitialRouter(self.onboardingRouter)
    }
}

