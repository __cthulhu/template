//
//  OnboardingRouter.swift
//  nxnews
//
//  Created by Anton Shelar on 18.03.2020.
//  Copyright © 2020 Anton Shelar. All rights reserved.
//

import Foundation
import UIKit


class OnboardingRouter: NXRouter {
    override func start() {
        let vm: OnboardingViewModel = OnboardingViewModel()

        let vc: OnboardingViewController = OnboardingViewController()
        vc.shouldHideNavigationBar = true
        vc.bindViewModel(vm)

        self.navigationController.setViewControllers([vc], animated: false)
    }

    func routeToFeed() { }
    func routeToCreateAccount() { }
    func routeToLogin() { }
}
