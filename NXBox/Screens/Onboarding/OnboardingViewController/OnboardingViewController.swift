//
//  OnboardingViewController.swift
//  nxnews
//
//  Created by Anton Shelar on 18.03.2020.
//  Copyright © 2020 Anton Shelar. All rights reserved.
//

import UIKit


class OnboardingViewController: NXViewController<OnboardingViewModel> {
    @IBOutlet private var gradientView: GradientView!
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var loginButton: BigButton!
    @IBOutlet private var createAccountButton: BigButton!
    @IBOutlet private var withoutAccountButton: BigButton!
    @IBOutlet private var textLabel: UILabel!
    @IBOutlet private var nextButton: BigButton!
    @IBOutlet private var pageControl: PageControlView!

    private let texts: [String] = [
        "doloren Lorem ipsum doloren sit amen",
        "sit amen Lorem ipsum doloren sit amen jgfrbcbhj",
        "Lorem ipsum sit amen doloren doloren doloren"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.gradientView.setColors([
            UIColor(red: 0.0, green: 11.0/255.0, blue: 16.0/255.0, alpha: 1.0).cgColor,
            UIColor.clear.cgColor,
        ])

        self.nextButton.setStyle(.solid(titleColor: .white, backgroundColor: R.color.neonRed()))
        self.nextButton.setTitle("NEXT", for: .init())

        self.loginButton.setStyle(.solid(titleColor: .black, backgroundColor: .white))
        self.loginButton.setTitle("LOGIN", for: .init())

        self.createAccountButton.setStyle(.solid(titleColor: .white, backgroundColor: R.color.neonRed()))
        self.createAccountButton.setTitle("CREATE ACCOUNT", for: .init())

        self.withoutAccountButton.setStyle(.plain(titleColor: .white))
        self.withoutAccountButton.setTitle("CONTINUE WITHOUT ACCOUNT", for: .init())
    }

    override func bindViewModel(_ vm: OnboardingViewModel) {
        super.bindViewModel(vm)

        self.pageControl.setPagesCount(Int(vm.totalPages))

        vm.currentPage.addObserver(owner: self) { [weak self] (index: Int) in
            self?.pageControl.setCurrentPage(index)
            self?.changeText(self?.texts[index])
        }

        vm.isAuthHidden.addObserver(owner: self) { [weak self] (authHidden: Bool) in
            self?.setAuthHidden(authHidden, animated: !authHidden)
        }

        self.nextButton.setTapAction { [weak vm] in
            vm?.didTapNextButton()
        }

        self.loginButton.setTapAction { [weak vm] in
            vm?.didTapLoginButton()
        }

        self.createAccountButton.setTapAction { [weak vm] in
            vm?.didTapCreateAccountButton()
        }

        self.withoutAccountButton.setTapAction { [weak vm] in
            vm?.didTapWithoutAccountButton()
        }
    }

    private func changeText(_ text: String?) {
        UIView.transition(with: self.textLabel,
                          duration: 0.25,
                          options: .transitionCrossDissolve, animations: {
            self.textLabel.text = text
        }, completion: nil)
    }

    private func setAuthHidden(_ hidden: Bool, animated: Bool) {
        let animations: (() -> Void) = {
            self.nextButton.isHidden = !hidden
            self.loginButton.isHidden = hidden
            self.createAccountButton.isHidden = hidden
            self.withoutAccountButton.isHidden = hidden
        }

        if animated {
            UIView.transition(with: self.stackView,
                              duration: 0.25,
                              options: .transitionCrossDissolve,
                              animations: animations,
                              completion: nil)
        } else {
            animations()
        }
    }
}
