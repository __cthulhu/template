//
//  OnboardingViewModel.swift
//  NXBox
//
//  Created by Anton Shelar on 24.04.2020.
//  Copyright © 2020 Anton Shelar. All rights reserved.
//

import Foundation


class OnboardingViewModel: NXViewModel {
    let totalPages: Int = 3
    let currentPage: NXObservable<Int> = NXObservable<Int>(0)
    let isAuthHidden: NXObservable<Bool> = NXObservable<Bool>(true)

    func didTapNextButton() {
        let newPageIndex: Int = self.currentPage.value + 1

        if (newPageIndex == self.totalPages) {
            self.isAuthHidden.value = false
        } else {
            self.currentPage.value = newPageIndex
        }
    }

    func didTapLoginButton() { }
    func didTapCreateAccountButton() { }
    func didTapWithoutAccountButton() { }
}
