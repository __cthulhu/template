//
//  AppDelegate.swift
//  NXBox
//
//  Created by Anton Shelar on 12/09/2019.
//  Copyright © 2019 Anton Shelar. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: NSObject, UIApplicationDelegate {
    var window: UIWindow?
    private var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {

        self.window = application.keyWindow ?? UIWindow(frame: UIScreen.main.bounds)

        self.appCoordinator = AppCoordinator(window: self.window!)
        self.appCoordinator.start()

        return true
    }
}

